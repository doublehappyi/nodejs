/**
 * Created by yishuangxi on 2015/6/2.
 */

var express = require('express')
var app = express()

app.set('views', './views')
app.set('view engine', 'jade')

app.get('/', function(req, res, next){
    res.render('index', {title:'This is title', message:'this is message'})
})
//使用express.Router
//var user = require('./user')
//app.use('/user', user)

/*app.all('/', function (req, res, next) {
    console.log('/ in app.all')
    next()
})

app.get('/', function (req, res, next) {
    console.log('hello world')
    next()
}, function(req, res, next){
    console.log('in 2nd callback')
    next()
}, function(req, res){
    console.log('in 3rd callback')
    //res.send('hello world')
    //res.json({name:'yisx', age:25})
    //res.jsonp({name:'yisx', age:25})
    //res.redirect('/login')
    res.sendStatus(200)
})



app.get('/login', function(req, res){
    res.send('This is login page')
})

app.use('/user/:id', function(req, res, next){
    //console.log('use /user/:id -->',req.originalUrl)
    //console.log('req.params.id: ',req.params.id)
    id = req.params.id
    console.log('id: ',id)
    next()

}, function(req, res, next){
    //console.log('use 2')
    next()
})

app.get('/user/:id', function(req ,res, next){
    //console.log('get /user/:id -->', req.method)
    //res.send('/user/:id')
    console.log("this is the first route !");
    console.log("this is first middleware in the first route")
    if(req.params.id == 0){
        console.log('next("route") is executed !')
        next('route')
    }else{
        console.log('next() is executed !')
        next()
    }
}, function(req, res, next){
    console.log("this is second middleware in the first route")
    res.send('this is the second middleware in the first route !')
})

app.get('/user/:id', function(req ,res, next){
    console.log('this is the second route !')
    res.send('this is the second route !')
})*/

app.listen(3000)