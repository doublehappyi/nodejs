/**
 * Created by yishuangxi on 2015/6/2.
 */
var express = require('express')
var router = express.Router()

router.use(function timelog(req, res, next) {
    console.log('use time: ', Date.now())
    next()
})

router.get('/', function (req, res) {
    res.send('this is /user/')
})

router.get('/login', function (req, res) {
    res.send('this is /user/login')
})

module.exports = router

//var express = require('express');
//var router = express.Router();
//
//// middleware specific to this router
//router.use(function timeLog(req, res, next) {
//    console.log('Time: ', Date.now());
//    next();
//});
//// define the home page route
//router.get('/', function(req, res) {
//    res.send('Birds home page');
//});
//// define the about route
//router.get('/about', function(req, res) {
//    res.send('About birds');
//});
//
//module.exports = router;
