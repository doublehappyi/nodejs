/**
 * Created by yishuangxi on 2015/6/3.
 */
var express = require('express')
var swig = require('swig')

var app = express()
app.use(express.static('static'))
app.set('views', './views')
app.engine('html', swig.renderFile)
app.set('view engine', 'html')


function User(name, email) {
    this.name = name
    this.email = email
}

var users = [
    new User('yishuangxi', 'yishuangxi@sina.com'),
    new User('kobe bryant', 'bryant@sina.com'),
    new User('Lebron James', 'lebron@sina.com')
]

app.get('/', function (req, res) {
    res.render('index', {users: users})
})

app.use(function (err, req, res, next) {
    res.send(err.stack)
})

app.listen(4000)