/**
 * Created by db on 15-6-1.
 */
var server = require("./server")
var router = require("./router")
var handler = require('./handler')

var requests = {
    '/': handler.index,
    '/upload': handler.upload
}

server.start(router.route, requests)