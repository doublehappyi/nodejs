/**
 * Created by db on 15-6-1.
 */
var http = require('http')
var router = require('./router')
var url = require('url')

function a(){}

function start(route, requests){
    http.createServer(function(request, response){
        var pathname = url.parse(request.url).pathname
        route(requests, pathname)
        response.writeHead(200, {'Content-Type':'text/plain'})
        response.write('pathname： ' + pathname)
        response.end()
    }).listen(8000)
}

exports.start = start